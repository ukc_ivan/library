<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;

/**
 * BookSearch represents the model behind the search form of `app\models\Book`.
 */
class Book extends ActiveRecord
{
    const SCENARIO_FILTER = 'filter';
    const SCENARIO_UPDATE = 'update';
    const SCENARIO_CREATE = 'create';

    /**
     * @var UploadedFile
     */
    public $uploadedFile;

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_FILTER] = ['id', 'page_count', 'name', 'cover', 'year', 'author.name', 'genre.name'];
        $scenarios[self::SCENARIO_UPDATE] = ['id', 'page_count', 'author_id', 'cover', 'genre_id', 'name', 'year', 'cover', 'file'];
        $scenarios[self::SCENARIO_CREATE] = ['id', 'page_count', 'author_id', 'cover', 'genre_id', 'name', 'year', 'cover'];
        return $scenarios;
    }

    public function rules()
    {
        return [
            [['id', 'page_count'], 'integer'],
            [['name', 'year'], 'safe'],
            [['author.name', 'genre.name'], 'safe', 'on'=> self::SCENARIO_FILTER],
            [['author_id', 'genre_id'], 'integer', 'on'=> self::SCENARIO_UPDATE],
            [['author_id', 'genre_id'], 'integer', 'on'=> self::SCENARIO_CREATE],
            [['uploadedFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'pdf', 'on' => self::SCENARIO_CREATE],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @pagination Pagination $pagination
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Book::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        $query->joinWith(['author' => function($query){
            $query->from(['author' => 'author']);
        }]);

        $query->joinWith(['genre' => function($query){
            $query->from(['genre' => 'genre']);
        }]);

        $dataProvider->sort->attributes['author.name'] = [
            'asc' => ['author.name' => SORT_ASC],
            'desc' => ['author.name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['genre.name'] = [
            'asc' => ['genre.name' => SORT_ASC],
            'desc' => ['genre.name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'year' => $this->year,
            'page_count' => $this->page_count,
            'genre_id' => $this->getAttribute('genre.name'),
            'author_id' => $this->getAttribute('author.name'),
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }

    public function getAuthor()
    {
        return $this->hasOne(Author::class, ['id' => 'author_id']);
    }

    public function getGenre()
    {
        return $this->hasOne(Genre::class, ['id' => 'genre_id']);
    }

    public function attributes()
    {
        return array_merge(parent::attributes(), ['author.name', 'genre.name']);
    }

    public function upload()
    {
        if (!is_dir('update/')){
            mkdir('update/', 0775);
        }
        $this->file = $this->uploadedFile->baseName . '.' . $this->uploadedFile->extension;
        $this->uploadedFile->saveAs('uploads/' . $this->file);

        $image = new \Imagick(Yii::getAlias('@webroot/uploads/' . $this->file . '[0]'));
        $image->writeImage(Yii::getAlias('@webroot/uploads/' . $this->uploadedFile->baseName . '.jpg'));
        $this->cover = $this->uploadedFile->baseName . '.jpg';
    }
}
