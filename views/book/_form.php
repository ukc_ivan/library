<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Author;
use app\models\Genre;

/* @var $this yii\web\View */
/* @var $model app\models\Book */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="book-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?php if ($showUploadField) {echo $form->field($model, 'file')->fileInput();} ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'author_id')->dropdownList(
            Author::find()->select(['name', 'id'])->indexBy('id')->column(),['prompt' => 'Выберите автора']
    ) ?>

    <?= $form->field($model, 'genre_id')->dropdownList(
            Genre::find()->select(['name', 'id'])->indexBy('id')->column(),['prompt' => 'Выберите жанр']
    ) ?>

    <?= $form->field($model, 'year')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'page_count')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
