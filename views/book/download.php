<?php
use yii\helpers\Url;

$class = $class ?? '';
?>

<a type="button" class="btn btn-success <?=$class?>" href="<?=Url::to(['book/download', 'file' => $file])?>">Download</a>