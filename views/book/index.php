<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Genre;
use app\models\Author;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BookSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Books';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (!Yii::$app->user->isGuest):?>
    <p>
        <?= Html::a('Create Book', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php endif; ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            [
                'label' => 'Author',
                'attribute' => 'author.name',
                'filter' => Author::find()->select(['name', 'id'])->indexBy('id')->column(),
                'filterInputOptions' => ['prompt' => 'Все авторы', 'class' => 'form-control']
            ],
            [
                'label' => 'Genre',
                'attribute' => 'genre.name',
                'filter' => Genre::find()->select(['name', 'id'])->indexBy('id')->column(),
                'filterInputOptions' => ['prompt' => 'Все жанры', 'class' => 'form-control']
            ],
            'year',
            'page_count',
            [
                'label' => 'Download',
                'attribute' => 'file',
                'content' => function($model){
                    return $this->render('download', ['file' => $model->file, 'class' => 'btn-xs']);
                },
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => !Yii::$app->user->isGuest,
                    'delete' => !Yii::$app->user->isGuest,
                ],
            ],
        ],
    ]); ?>

</div>
